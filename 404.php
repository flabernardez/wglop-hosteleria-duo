<?php get_header(); ?>

    <main id="content" class="site-content">

                <article id="post-e404">

                    <section class="entry-content">

                        <p>

                        <?php

                        $link_home = get_home_url();

                        printf(

	                        esc_html__( 'This page does not exist. Please, try to %1$s or we let you content that is already created:', "wglop-hosteleria-duo-theme" ),
                            sprintf(
                                    '<a href="%s" title="%s">%s</a>',
	                            $link_home,
	                            esc_html__( 'Home page', 'wglop-hosteleria-duo-theme' ),
	                            esc_html__( 'visit home page', 'wglop-hosteleria-duo-theme' )
                            )

                        );

                        ?>

                        </p>

                        <ul class="lista-404">

                        <?php
                            wp_list_pages(array('sort_column' => 'post_date', 'sort_order' => 'desc', 'title_li' => ''));
                        ?>

                        </ul>

                    </section><!-- .entry-content -->

                </article><!-- #post-<?php the_ID(); ?> -->

    </main><!-- #content -->

<?php get_footer();