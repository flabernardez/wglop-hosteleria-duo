// Login form dropdown
$(function() {
    $('#login-form').hide();
    $('#form-toggle').click(function() {
        $('#login-form').toggle();
    });
});

// Cart dropdown
$(function() {
    $('#order-list').hide();
    $('#order-toggle').click(function() {
        $('#order-list').toggle();
    });
});

// Mobile category dropdown
$(function() {
    $('#category-list-rest').hide();
    $('#category-list-toggle').click(function() {
        $('#category-list-rest').toggle();
    });
});