<?php
/**
 * Template for displaying search forms in Glop Hosteleria Duo Theme
 *
 * @package WordPress
 * @subpackage Cancer_Theme
 * @since Glop Hosteleria Duo Theme 1.1
 */
?>
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text"><?php ' ' . _x( 'Search for:', 'label' );?></span>
		<input type="search" class="search-field" placeholder="<?php esc_attr_e( 'Search for...', 'wglop-hosteleria-duo-theme' ) . '" value="' . get_search_query()?>" name="s" />
	</label>
	<input type="submit" class="search-submit emoji" value="🔎" />
</form>

