
<footer id="colophon" class="site-footer">

	<article class="wrapper">

        <?php
		if ( is_active_sidebar( 'footer-widget' ) ) : ?>

            <section class="footer-widget-area">
				<?php dynamic_sidebar( 'footer-widget' ); ?>
			</section>

		<?php
		endif;

		if ( has_nav_menu( 'menu-2' ) ) : ?>
            <nav class="footer-navigation">
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'menu-2',
                    'menu_id'        => 'footer-menu',
                ) );
                ?>
            </nav><!-- #site-navigation -->
		<?php
        endif; ?>

        <section class="footer-site-name">
			<?php

			echo '<p>' . get_bloginfo( 'name' ) . ' ' . '| Web hecha por <a href="https://www.glop.es">glop.es</a> © '  . date("Y") . '</p>';

			?>
        </section>

	</article>

</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
