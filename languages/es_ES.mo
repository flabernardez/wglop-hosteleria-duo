��            )   �      �  "   �     �     �     �  =   �  	   %     /     8     ?     F  	   K     U     Z     `  !   i     �     �     �     �     �     �  \   �     +     B  \   [  \   �  r        �  �  �  &   w     �     �     �  F   �  	   "	  	   ,	     6	     E	     N	     `	     r	     w	     ~	  #   �	     �	  	   �	     �	     �	      �	     �	  �   �	     u
     �
  h   �
  |     �   �                     	                  
                                                                                                         %1$s thought on &ldquo;%2$s&rdquo; Accent color Add widget here. Comments are closed. Continue reading<span class="screen-reader-text"> "%s"</span> Error 404 Featured Footer Header Home Home page Info Login More ⌄ One thought on &ldquo;%1$s&rdquo; Order Pages: Search for... Search for: Search results for: Sign in Sorry, but nothing matched your search terms. Please try again with some different keywords. This is the Front Page This is the Order Widget This page does not exist. Please, try to %1$s or we let you content that is already created: You need to add any widget in Home Widget Area in WordPress Dashboard > Appearence > Widgets You need to add the WooCommerce cart widget inside Order Widget Area in WordPress Dashboard > Appearence > Widgets visit home page Project-Id-Version: Glop Hosteleria Theme
POT-Creation-Date: 2019-07-15 08:18+0200
PO-Revision-Date: 2019-07-15 08:22+0200
Last-Translator: 
Language-Team: flabernardez.com
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.3
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;_x;_ex;_n;_nx;_n_noop;_nx_noop;translate_nooped_plural;esc_html__;esc_html_e;esc_html_x;esc_attr__;esc_attr_e;esc_attr_x;number_format_i18n;date_i18n
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: js
X-Poedit-SearchPathExcluded-1: scss
X-Poedit-SearchPathExcluded-2: config.codekit3
X-Poedit-SearchPathExcluded-3: screenshot.png
 %1$s comentarios en &ldquo;%2$s&rdquo; Color principal Añadir widgets aquí. Comentarios cerrados. Continúa leyendo<span class=“screen-reader-text”> “%s”</span> Error 404 Destacado Pie de página Cabecera Página de inicio Página de inicio Info Entrar Más ⌄ Un comentario en &ldquo;%1$s&rdquo; Pedido Páginas: Busca… Busca: Resultados de la búsqueda para: Entrar Lo siento, pero no hemos encontrado coincidencia con los términos de búsqueda. Prueba de nuevo con diferentes palabras claves. Esto es la página principal Esto es el Order Widget Area ¡Anda! Esta página no existe. Prueba a %1$s o a continuación te dejo contenido interesante ya creado: Necesitas añadir algún widget en el Área de Widget de la página de inicio en WordPress Escritorio > Apariencia > Widgets Necesitas añadir el Widget del carrito de WooCommerce dentro del Área de widget de pedido en WordPress Escritorio > Apariencia > Widgets visita la página de inicio 