#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Glop Hosteleria Duo Theme\n"
"POT-Creation-Date: 2019-07-15 08:18+0200\n"
"PO-Revision-Date: 2019-01-23 10:27+0100\n"
"Last-Translator: \n"
"Language-Team: flabernardez.com\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.3\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-KeywordsList: __;_e;_x;_ex;_n;_nx;_n_noop;_nx_noop;translate_nooped_plural;esc_html__;esc_html_e;esc_html_x;esc_attr__;esc_attr_e;esc_attr_x;number_format_i18n;date_i18n\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: js\n"
"X-Poedit-SearchPathExcluded-1: scss\n"
"X-Poedit-SearchPathExcluded-2: config.codekit3\n"
"X-Poedit-SearchPathExcluded-3: screenshot.png\n"

#: 404.php:17
#, php-format
msgid "This page does not exist. Please, try to %1$s or we let you content that is already created:"
msgstr ""

#: 404.php:21
msgid "Home page"
msgstr ""

#: 404.php:22
msgid "visit home page"
msgstr ""

#: comments.php:34
#, php-format
msgid "One thought on &ldquo;%1$s&rdquo;"
msgstr ""

#: comments.php:40
#, php-format
msgid "%1$s thought on &ldquo;%2$s&rdquo;"
msgstr ""

#: comments.php:63
msgid "Comments are closed."
msgstr ""

#: front-page.php:27
msgid "More ⌄"
msgstr ""

#: front-page.php:68 index.php:27
#, php-format
msgid "Continue reading<span class=\"screen-reader-text\"> \"%s\"</span>"
msgstr ""

#: front-page.php:96
msgid "This is the Order Widget"
msgstr ""

#: front-page.php:97
msgid "You need to add the WooCommerce cart widget inside Order Widget Area in WordPress Dashboard > Appearence > Widgets"
msgstr ""

#: functions.php:20
msgid "Header"
msgstr ""

#: functions.php:24 functions.php:214
msgid "Footer"
msgstr ""

#: functions.php:56
msgid "Accent color"
msgstr ""

#: functions.php:216 functions.php:226 functions.php:236 functions.php:246
#: functions.php:256
msgid "Add widget here."
msgstr ""

#: functions.php:224
msgid "Login"
msgstr ""

#: functions.php:234
msgid "Order"
msgstr ""

#: functions.php:244
msgid "Info"
msgstr ""

#: functions.php:254
msgid "Home"
msgstr ""

#: header.php:57
msgid "Sign in"
msgstr ""

#: header.php:82
msgid "This is the Front Page"
msgstr ""

#: header.php:83
msgid "You need to add any widget in Home Widget Area in WordPress Dashboard > Appearence > Widgets"
msgstr ""

#: header.php:90
msgid "Error 404"
msgstr ""

#: header.php:95
msgid "Search results for:"
msgstr ""

#: index.php:38
msgid "Pages:"
msgstr ""

#: search.php:20
msgid "Featured"
msgstr ""

#: search.php:42
msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr ""

#: searchform.php:12
msgid "Search for:"
msgstr ""

#: searchform.php:13
msgid "Search for..."
msgstr ""
