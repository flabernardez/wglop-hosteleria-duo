<?php

get_header(); ?>

	<main id="content" class="site-content">
        <section class="wrapper">
            <nav id="menu-sidebar" class="sidebar-navigation">
                <h2><?php _e( 'Categories', 'wglop-hosteleria-duo-theme' )?></h2>
                <ul class="category-list">

                    <?php

                    $args = array(
                        'taxonomy'     => 'product_cat',
                        'orderby'      => 'count',
                        'title_li'     => '',
                        'hide_empty'   => 0
                    );
                    $all_categories = get_categories( $args );
                    foreach ($all_categories as $cat) {
                        if($cat->category_parent == 0 ) {
                            echo '<li><a href="#'. $cat->slug .'">'. $cat->name .'</a></li>';
                        }
                    }
                    ?>
                </ul>
            </nav>
	        <?php
	        if ( have_posts() ) :

            /* Start the Loop */
            while ( have_posts() ) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <section class="entry-content">

                    <?php

                    the_content( sprintf(
                        wp_kses(
                        /* translators: %s: Name of current post. Only visible to screen readers */
                            __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'wglop-hosteleria-duo-theme' ),
                            array(
                                'span' => array(
                                    'class' => array(),
                                ),
                            )
                        ),
                        get_the_title()
                    ) );
                    ?>

                </section><!-- .entry-content -->
            </article><!-- #post-<?php the_ID(); ?> -->

            <?php
            endwhile;

	        endif;

	        ?>
        </section>
	</main><!-- #content -->

<?php

get_footer(); ?>